
import React, { useState } from 'react';
import Split from 'react-split';
import './../styles/style.scss';
import EmptyComponent from './EmptyComponent'
import ItemInsert from './ItemInsert'
import DataFilter from './DataFilter';
import CalculateSize from './CalculateSize';
import { connect } from 'react-redux';
import './../styles/style.scss';
import { calculateHorizontal,calculateTopVertical,calculateBottomVertical } from '../actions';
const SplitComp = props => {
    const handleChangeHorizontal = sizeHorizontal => {
        props.calculateHorizontal(sizeHorizontal)
    };
    const handleChangeTopVertical = sizeTopVertical => {
        props.calculateTopVertical(sizeTopVertical)
    };
    const handleChangeBottomVertical = sizeBottomVertical => {
        props.calculateBottomVertical(sizeBottomVertical)
    };
    return (
        <Split
            className="main-container"
            direction="vertical"
            dragInterval={2}
            gutterSize={5}
            sizes={[60, 40]}
            onDragEnd={handleChangeHorizontal}
        >
            <Split
                className="row-container"
                dragInterval={2}
                gutterSize={5}
                sizes={[70, 30]}
                onDragEnd={handleChangeTopVertical}
            >
                <div><DataFilter /></div>
                <div><CalculateSize /></div>
            </Split>
            <Split
                className="row-container"
                dragInterval={2}
                gutterSize={6}
                sizes={[70, 30]}
                onDragEnd={handleChangeBottomVertical}
            >
                <div><ItemInsert /></div>
                <div><EmptyComponent /></div>
            </Split>
        </Split>
    )
}
const mapStateProps = state => {
    return {
        newList: state.newList,
        list: state.list,
        sizeBottomVertical: state.sizeBottomVertical,
        sizeHorizontal: state.sizeHorizontal,
        sizeTopVertical : state.sizeTopVertical
    }
}
export default connect(mapStateProps, { calculateHorizontal,calculateTopVertical,calculateBottomVertical })(SplitComp);


