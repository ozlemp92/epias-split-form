
import React, { useState } from 'react';
import { connect } from 'react-redux';
import { Table, Button, Form, Popover, OverlayTrigger, Dropdown } from 'react-bootstrap';
import { useSelector } from 'react-redux';
import { default as setting } from '../assests/settings.svg';
import { default as up } from '../assests/up-arrow.svg';
import { default as add } from '../assests/add.svg';
import './../styles/style.scss';
const DataFilter = props => {
    //const gridDatas = useSelector(state => state.gridData)
    const [visiblity, setVisibility] = useState({ id: true, kontrat: true, teklif: true, data: true });
    const [selectedValue, setSelect] = useState({ value: "" });
    const [allData, setDataGrid] = useState({
        gridData: [
            {
                id: 1,
                kontrat: "2018",
                teklif: "1000",
                data: "Satış"
            },
            {
                id: 2,
                kontrat: "2020",
                teklif: "5600",
                data: "Alış"
            },
            {
                id: 3,
                kontrat: "2020",
                teklif: "5600",
                data: "Alış"
            },
            {
                id: 4,
                kontrat: "2018",
                teklif: "5600",
                data: "Alış"
            },
        ]
    });
    function handleClick(e) {
        const { checked, value } = e.target;
        setVisibility((prevState) => ({
            ...prevState,
            ...prevState,
            [value]: checked
        }));
    }
    function selectChange(e) {
        setSelect(e.target.value);
        const gridData= [
            {
                id: 1,
                kontrat: "2018",
                teklif: "1000",
                data: "Satış"
            },
            {
                id: 2,
                kontrat: "2020",
                teklif: "5600",
                data: "Alış"
            },
            {
                id: 3,
                kontrat: "2020",
                teklif: "5600",
                data: "Alış"
            },
            {
                id: 4,
                kontrat: "2018",
                teklif: "5600",
                data: "Alış"
            },
        ]
        allData.gridData =gridData;
        var data = allData.gridData.filter(x => x.kontrat == e.target.value);
        allData.gridData = data;
        setDataGrid((prevState) => ({
            ...prevState,
            ...prevState,
            gridData: data
        }));
        //  setDataGrid(allData);
    }

    const popover = (
        <Popover id="filter-popup">
            <Popover.Content>
                <Form>
                    <Form.Check label="id" value="id" checked={visiblity.id} onClick={handleClick} />
                    <Form.Check label="kontrat" value="kontrat" checked={visiblity.kontrat} onClick={handleClick} />
                    <Form.Check label="teklif" value="teklif" checked={visiblity.teklif} onClick={handleClick} />
                    <Form.Check label="data" value="data" checked={visiblity.data} onClick={handleClick} />
                </Form>
            </Popover.Content>
        </Popover>
    );
    let datas = allData && allData.gridData.map(item => item.kontrat)
        .filter((value, index, self) => self.indexOf(value) === index);
    let MakeItem = function (X) {
        return <option selected={true} value={X}>{X}</option>;
    };

    return (
        <div>
            <div>
                <div className="float-left">

                    <select value={selectedValue} onChange={selectChange} >
                        <option value="">Seçiniz</option>
                        <option value="2018">2018</option>
                        <option value="2020">2020</option>
                    </select>
                </div>
                <div  className="float-padding">
                <img className="main-icon" src={up} />
                    <OverlayTrigger trigger="click" placement="bottom" overlay={popover}>
                        <Button variant="link">
                            <img className="main-icon" src={setting} />                     
                        </Button>
                    </OverlayTrigger>             
                    <img className="main-icon" src={add} />
                </div>
            </div>
            <Table striped bordered hover size="sm">
                <thead>
                    <tr>
                        {visiblity.id && <th>Id</th>}
                        {visiblity.kontrat && <th>Kontrat</th>}
                        {visiblity.teklif && <th>Teklif</th>}
                        {visiblity.data && <th>Data</th>}
                    </tr>
                </thead>
                <tbody>
                    {allData && allData.gridData.map(item =>
                    (
                        <tr>
                            {visiblity.id &&
                                <td>{item.id}</td>
                            }
                            {visiblity.kontrat &&
                                <td>{item.kontrat}</td>
                            }
                            {visiblity.teklif &&
                                <td>{item.teklif}</td>
                            }
                            {visiblity.data &&
                                <td>{item.data}</td>
                            }
                        </tr>
                    ))
                    }
                </tbody>
            </Table>
        </div>
    )
}




export default DataFilter;